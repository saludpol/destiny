response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description
# response.menu = [
# (T('Index'),URL('default','index')==URL(),URL('default','index'),[]),
# ]

home = [(T('Inicio'), False, URL('default','index'), [])]

internal_home = home

configuracion = [
    (T('Configuración'), False, None,
     [
        (T('Áreas'),False,URL('config','areas'),[]),
        (T('Documentos'),False,URL('config','documentos'),[]),
        (T('Prioridades'),False,URL('config','prioridades'),[]),
        (T('Tipos de trámites'),False,URL('config','tipos_tramites'),[]),
        (T('Indicaciones'),False,URL('config','indicaciones'),[]),
        (T('Administración de usuarios'), False, URL('config', 'users_manage'),[]),
        (T('Grupos de usuario'), False, URL('config', 'users_groups'),[]),
        (T('Membresías de usuario'), False, URL('config', 'users_membership'),[]),
     ]
    ),
]

archivos = [
    (T('Archivos'), False, URL('archivos', 'lista_archivos'), []),
]

procesos = [
    (T('Procesos'), False, None,
     [
        (T('Nuevo trámite'),False,URL('procesos','nuevo_tramite'),[]),
        (T('Procesamiento de trámites'),False,URL('procesos','desarrollo_tramite'),[]),
     ]
    ),
]

tramites = [
    (T('Trámites'), False, None,
     [
        (T('Consultas'),False,URL('tramites','consulta_tramite'),[]),
     ]
    ),
]

reportes = [
    (T('Reportes'), False, None,
     [
        (T('Eventos de usuario'), False, URL('reportes', 'eventos_usuario'),[]),
     ]
    ),
]

response.menu = home
#response.menu += services
if auth.user:
    user = auth.user.id
    if auth.has_membership(user_id=user, role='root'):
        response.menu += archivos
        response.menu += configuracion
        response.menu += procesos
        response.menu += reportes
        response.menu += tramites
    elif auth.has_membership(user_id=user, role='user'):
        response.menu += archivos
        response.menu += procesos
        response.menu += tramites
    else:
        #response.menu += configuration
        response.menu += tramites
