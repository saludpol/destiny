# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

DEVELOPMENT = False
if request.env.http_host in ['127.0.0.1:8000', 'localhost:8000']:
    DEVELOPMENT = True

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL("postgres://saludpol:y2KSalud14@localhost:5432/destiny")
    #db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

import datetime
from gluon.tools import Auth, Crud, Service, PluginManager, prettydate

cas_provider = 'http://192.168.0.246/provider/default/user/cas'
if DEVELOPMENT:
    cas_provider = 'http://localhost:8000/provider/default/user/cas'

auth = Auth(db, cas_provider=cas_provider)
crud, service, plugins = Crud(db), Service(), PluginManager()
T.force('es')

## create all tables needed by auth if not custom tables
#auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True
auth.settings.create_user_groups = None

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

mail.settings.server = settings.email_server
mail.settings.sender = settings.email_sender
mail.settings.login = settings.email_login

now = datetime.datetime.now()


status_options = {
    0: T('Activo'),
    1: T('Archivado')
}


db.define_table('tipos_tramites',
    Field('nombre', 'string', unique=True, label=T('Nombre')),
    Field('dias', 'integer', label=T('Dias'), default=7),
    format='%(nombre)s'
)

db.define_table('areas',
    Field('nombre', 'string', unique=True, label=T('Nombre')),
    Field('siglas', 'string', default='', label=T('Siglas')),
    format='%(nombre)s'
)

db.define_table('indicaciones',
    Field('nombre', 'string', unique=True, label=T('Nombre')),
    format='%(nombre)s'
)

auth.settings.extra_fields['auth_user'] = [
    Field('area', db.areas, label=T('Área'), requires=IS_EMPTY_OR(IS_IN_DB(db, db.areas, '%(nombre)s'))),
]

auth.define_tables(username=False, signature=False)

db.define_table('documento_numeracion',
    Field('tipo_documento', db.tipos_tramites, label=T('Tipo Documento')),
    Field('area', db.areas, label=T('Área')),
    Field('numero', 'integer', label=T('Número'), default=1),
    Field('ano', 'integer', label=T('Año'), default=now.year),
)

db.define_table('documento_generado',
    Field('documento_numeracion', db.documento_numeracion),
    Field('numero_generado', 'string', label=T('Número generado')),
    Field('pendiente', 'boolean', label=T('Pendiente'), default=True),
    Field('usuario', db.auth_user, label=T('Usuario')),
    format=lambda r: r.numero_generado
)

db.define_table('prioridades',
    Field('nombre', 'string', unique=True, label=T('Nombre')),
    Field('nivel', 'integer', label=T('Nivel')),
    format='%(nombre)s'
)

db.define_table('tramites',
    Field('tiempo', 'datetime', label=T('Fecha'), default=now, writable=False),
    Field('documento_generado', db.documento_generado, label=T('Documento')),
    Field('referencia', 'string', label=T('Referencia SIGE')),
    Field('prioridad', db.prioridades, label=T('Prioridad'), default=1),
    Field('area', db.areas, label=T('Área')),
    Field('resumen', 'string', label=T('Asunto'), requires=IS_NOT_EMPTY()),
    Field('estado', 'integer', label=T('Estado'), default=0, requires=IS_IN_SET(status_options)),
    format='%(id)s'
)

db.define_table('tramites_procesos',
    Field('tiempo', 'datetime', label=T('Fecha'), default=now, writable=False),
    Field('tramite', db.tramites, label=T('Nº Trámite')),
    Field('documento_generado', db.documento_generado, label=T('Documento')),
    Field('indicacion', 'list:reference indicaciones', label=T('Indicaciones'), requires=IS_IN_DB(db, db.indicaciones, '%(nombre)s', multiple=True)),
    Field('usuario', db.auth_user, label=T('Usuario')),
    Field('documento', 'upload', label=T('Archivo')),
    Field('nombre_documento', 'string', label=T('Nombre Documento'), writable=False),
    Field('area_destino', 'list:reference areas', label=T('Unidades Destino'), requires=IS_EMPTY_OR(IS_IN_DB(db, db.areas, '%(nombre)s', multiple=True))),
    Field('usuario_destino', db.auth_user, label=T('Usuario Destino'), requires=IS_EMPTY_OR(IS_IN_DB(db, db.auth_user, '%(last_name)s %(first_name)s'))),
    Field('comentarios', 'text', label=T('Comentarios')),
    Field('estado', 'integer', label=T('Estado'), default=0, requires=IS_IN_SET(status_options)),
    format='%(tramite)s - %(id)s'
)

db.define_table('archivos',
    Field('archivo', 'upload', label=T('Archivo')),
    Field('nombre_archivo', 'string', label='Nombre archivo'),
    Field('usuario', db.auth_user, label=T('Usuario')),
    Field('notas', 'text', label=T('Notas')),
    Field('oculto', 'boolean', default=False),
    format=lambda row: A(row.nombre_archivo, _href=URL('download', args=row.archivo)),
)

db.define_table('archivos_compartidos',
    Field('archivo', db.archivos, label='Archivo'),
    Field('compartido_con', 'list:reference auth_user', label='Compartir con', requires=IS_IN_DB(db, db.auth_user, '%(last_name)s, %(first_name)s', multiple=True)),
    Field('mensaje', 'text', label='Mensaje'),
)

def name_data(table, id_val, field='%(nombre)s'):
    """
    Content Data Representation

    """
    try:
        value_data = table[id_val]
        data = field % value_data
    except:
        data = ''
    data = data.decode('utf8')
    return data

def cambio_estado(values, id):
    tramite = values['tramite']
    estado = values['estado']
    db(db.tramites.id==tramite).update(estado=estado)
    return


db.auth_user._format = '%(last_name)s, %(first_name)s'
db.auth_user.area.represent = lambda value, row: '' if value is None else name_data(db.areas, value)
db.tramites.id.represent = lambda value, row: str(value).zfill(4)
db.tramites.id.label = T('Nº Trámite')
db.tramites.tiempo.represent = lambda value, row: value.strftime('%d/%m/%Y %H:%M:%S')
db.tramites.prioridad.represent = lambda  value, row: '' if value is None else name_data(db.prioridades, value)
db.tramites.area.represent = lambda  value, row: '' if value is None else name_data(db.areas, value)
db.tramites.estado.represent = lambda  value, row: '' if value is None else status_options[value]
db.tramites_procesos.id.represent = lambda value, row: str(value).zfill(2)
db.tramites_procesos.id.label = T('Nº Proceso')
db.tramites_procesos.tramite.represent = lambda value, row: str(value).zfill(4)
db.tramites_procesos.tiempo.represent = lambda value, row: value.strftime('%d/%m/%Y %H:%M:%S')
db.tramites_procesos.usuario_destino.represent = lambda  value, row: '' if value is None else name_data(db.auth_user, value, '%(last_name)s, %(first_name)s')
db.tramites_procesos.estado.represent = lambda  value, row: '' if value is None else status_options[value]
db.tramites_procesos._after_insert.append(lambda values, id: cambio_estado(values, id))
db.tramites_procesos.usuario_destino.widget = lambda field,value: autocomplete_widget(field,value,base='auth_user', content='last_name,first_name')
db.tramites_procesos.indicacion.represent = lambda value, row: UL(map(lambda i: LI(i.nombre), value))
db.tramites_procesos.area_destino.represent = lambda  value, row: UL(map(lambda i: LI(i.nombre), value))
db.tramites_procesos.documento_generado.represent = lambda value, row: '-' if value is None else name_data(db.documento_generado, value, field='%(numero_generado)s')
db.tramites_procesos.nombre_documento.represent = lambda value, row: '-' if value is None else value
db.tramites_procesos.documento.represent = lambda value, row: A('%s' % db.tramites_procesos(db.tramites_procesos.id==row.id)['nombre_documento'], _href=URL('download', args=value))
db.archivos.archivo.represent = lambda value, row: A('%s' % db.archivos(db.archivos.id==row.id)['nombre_archivo'], _href=URL('download', args=value))
