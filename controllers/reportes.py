# -*- coding: utf-8 -*-
### required - do no delete
def user():
    return dict(form=auth())

def download():
    return response.download(request,db)

def call():
    return service()
### end requires

def error():
    return dict()

def get_event_description(evento):
    return ' '.join(evento.split()[2:])

@auth.requires_login()
def eventos_usuario():
    eventos = None
    db.auth_event.user_id.label = 'Usuario'
    db.auth_event.time_stamp.label = 'Fecha'
    db.auth_event.description.label = 'Evento'
    db.auth_event.description.represent = lambda v, r: get_event_description(v)

    form_user = SQLFORM.factory(
        Field('usuario',
            db.auth_user,
            label='Usuario',
            requires=IS_EMPTY_OR(IS_IN_DB(db, db.auth_user, '%(last_name)s, %(first_name)s', zero='---Seleccionar---')),
        ),
        _class='form-inline',
        submit_button='Generar',
        formstyle='inline',
    )
    form_user.element('#no_table_usuario')['_class'] = 'span4'
    form_user.element(_type='submit')['_class'] = 'btn btn-success'

    if form_user.process().accepted:
        user = form_user.vars.usuario
        if user:
            eventos = SQLFORM.grid(
                db(db.auth_event.user_id==user).query,
                fields=[
                    db.auth_event.user_id,
                    db.auth_event.time_stamp,
                    db.auth_event.description
                ],
                editable=False,
                deletable=False,
                create=False,
                details=False,
                searchable=False,
                sortable=False,
                maxtextlength=50,
                paginate=100,
                orderby=~db.auth_event.time_stamp,
                exportclasses=dict(xml=False, html=False, json=False, tsv=False, tsv_with_hidden_cols=False),
            )
            if eventos.element('.web2py_htmltable table'):
                eventos.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
            response.flash = None
        else:
            response.flash = SPAN('Debe seleccionar un usuario', _class='text-error')

    return dict(form_user=form_user, eventos=eventos)
