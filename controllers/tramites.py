# -*- coding: utf-8 -*-
from datetime import datetime

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def days_hours(td):
    return DIV(DIV('%s día(s)' % td.days), DIV('%s hora(s)' % (td.seconds//3600)))

@auth.requires_login()
def consulta_desarrollo_tramite():
    search = request.vars['keywords']
    tramite = db.tramites(db.tramites.id==request.args(0))

    procesos = db(db.tramites_procesos.tramite==tramite).select(
        db.tramites_procesos.tramite,
        db.tramites_procesos.tiempo,
        db.tramites_procesos.indicacion,
        db.tramites_procesos.documento_generado,
        db.tramites_procesos.documento,
        db.tramites_procesos.usuario,
        db.tramites_procesos.area_destino,
        db.tramites_procesos.usuario_destino,
        db.tramites_procesos.comentarios,
    )
    if search:
        procesos = db(
            (db.tramites_procesos.tramite==tramite) & (
                (db.tramites_procesos.comentarios.like('%%%s%%' % search)) | \
                (db.tramites_procesos.documento_generado.like('%%%s%%' % search))
            )
        ).select(
            db.tramites_procesos.tramite,
            db.tramites_procesos.tiempo,
            db.tramites_procesos.indicacion,
            db.tramites_procesos.documento_generado,
            db.tramites_procesos.documento,
            db.tramites_procesos.usuario,
            db.tramites_procesos.area_destino,
            db.tramites_procesos.usuario_destino,
            db.tramites_procesos.comentarios,
        )

    procesos_data = []
    counter = 1
    for proceso in procesos:
        # Indicaciones
        indicaciones = []
        for indicacion in proceso.indicacion:
            indicaciones.append(LI(indicacion.nombre))
        indicaciones = UL(*indicaciones)

        # Areas
        areas = []
        for area in proceso.area_destino:
            areas.append(LI(area.nombre))
        areas = UL(*areas)

        # Usuario destino
        usuario_destino = '-'
        if proceso.usuario_destino:
            usuario_destino = '%s, %s' % (proceso.usuario.last_name, proceso.usuario.first_name)

        # Demoras
        if counter < len(procesos):
            demora = days_hours(procesos[counter].tiempo - proceso.tiempo)
        else:
            if not proceso.tramite.estado:
                demora = days_hours(datetime.now() - proceso.tiempo)
            else:
                demora = '-'

        # Documento generado
        documento_generado = '-'
        if proceso.documento_generado:
            documento_generado = proceso.documento_generado.numero_generado

        data = {
            'tramite': proceso.tramite,
            'counter': counter,
            'tiempo': proceso.tiempo.strftime('%d/%m/%Y %H:%M:%S'),
            'indicacion': indicaciones,
            'documento_generado': documento_generado,
            'documento': proceso.documento,
            'usuario': '%s, %s' % (proceso.usuario.last_name, proceso.usuario.first_name),
            'area_destino': areas,
            'usuario_destino': usuario_destino,
            'comentarios': proceso.comentarios,
            'demora': demora,
        }
        procesos_data.append(data)
        counter += 1
    return dict(procesos_data=procesos_data, tramite=tramite, search=search)

@auth.requires_login()
def consulta_tramite():
    user_id = auth.user.id
    user_area = auth.user.area
    form = SQLFORM.grid(
        db(db.tramites).query,
        maxtextlength=100,
        editable=False,
        deletable=False,
        create=False,
        details=False,
        exportclasses=dict(xml=False, html=False, json=False, tsv=False, tsv_with_hidden_cols=False),
        orderby=~db.tramites.id,
        links=[dict(
            header='Acciones', body=lambda row: DIV(A(IMG(_src=URL('static', 'images/list_icon.png'), _title='Ver desarrollo'), _href=URL('tramites', 'consulta_desarrollo_tramite', args=row.id)))
        )]
    )

    if form.element('.web2py_table table'):
        form.element('.web2py_table table')['_class'] = 'table table-bordered'
        for tr in form.element('.web2py_table table tbody'):
            for td in tr.components:
                if 'Urgente' in td.components:
                    td.add_class('warning warning-message')
                elif 'Muy Urgente' in td.components:
                    td.add_class('danger danger-message')
    return dict(form=form)

