# -*- coding: utf-8 -*-
import datetime
from cmislib import CmisClient

# try:
#     client = CmisClient('http://192.168.1.33/alfresco/api/-default-/public/cmis/versions/1.0/atom', 'admin', 'admin')
#     repo = client.defaultRepository
#     ALFRESCO = True
# except:
#     ALFRESCO = False
ALFRESCO = False
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()


def procesamiento_form(form):
    area_destino = form.vars.area_destino
    usuario_destino = form. vars.usuario_destino
    if area_destino is None and usuario_destino is None:
        error_message = u'Al menos un usuario o area destino se necesita seleccionar'
        form.errors.a = error_message
        response.flash = error_message
#    else:
#        form.vars.c = c

@auth.requires_login()
def nuevo_tramite():
    user_id = auth.user.id
    user_area = auth.user.area
    documento_numeracion_id = None

    # Formulario de generación de documentos
    form_document = SQLFORM.factory(
        Field('tipo_documento', db.tipos_tramites, label=T('Tipo Documento'),
            requires=IS_IN_DB(db, db.tipos_tramites, '%(nombre)s', zero='---Seleccionar---', error_message='Debe seleccionar un tipo de documento')),
        _name='form-document',
        submit_button='Generar',
        formstyle='bootstrap',
    )
    form_document.element('#no_table_tipo_documento')['_class'] = 'span6'
    form_document.element(_type='submit')['_class'] = 'btn btn-success'

    if form_document.process().accepted:
        tipo_documento = form_document.vars.tipo_documento
        area = auth.user.area
        if area:
            documento_numeracion_id = generacion_documento(tipo_documento, area)
            response.flash = 'Se generó el siguiente documento: %s' % documento_numeracion_id.numero_generado
        else:
            response.flash = 'ERROR: ¡Usuario sin área!'

    form = SQLFORM.factory(
        Field('documento_generado', db.documento_generado,
            label=T('Documento'),
            # requires=IS_IN_DB(db, db.documento_generado, '%(numero_generado)s',
            requires=IS_IN_DB(db((db.documento_generado.pendiente==True) & (db.documento_generado.usuario==user_id)), db.documento_generado, '%(numero_generado)s', zero='---Seleccionar---', error_message='Este campo es obligatorio'),
            default=documento_numeracion_id),
        Field('indicacion', 'list:reference indicaciones', label=T('Indicaciones'), requires=IS_IN_DB(db, db.indicaciones, '%(nombre)s', multiple=True)),
        Field('referencia', 'string', label=T('Referencia SIGE')),
        Field('prioridad', db.prioridades, label=T('Prioridad'), default=1, requires=IS_EMPTY_OR(IS_IN_DB(db, db.prioridades, '%(nombre)s'))),
        Field('resumen', 'string', label=T('Asunto'), requires=IS_NOT_EMPTY(error_message='Este campo es obligatorio')),
        Field('documento', 'upload', label=T('Documento'), uploadfolder=UPLOAD_PATH, requires=IS_NOT_EMPTY(error_message='Debe adjuntar un archivo')),
        Field('area_destino', 'list:reference areas', label=T('Unidades Destino'), requires=IS_EMPTY_OR(IS_IN_DB(db, db.areas, '%(nombre)s', multiple=True))),
        Field('usuario_destino', db.auth_user, label=T('Usuario Destino'),
              requires=IS_EMPTY_OR(IS_IN_DB(db, db.auth_user, '%(last_name)s %(first_name)s')),
              widget=lambda field,value: autocomplete_widget(field,value,base='auth_user',
                content='last_name,first_name')),
        Field('comentarios', 'text', label=T('Comentarios')),
        table_name='tramites_procesos',
        formstyle='bootstrap',
        submit_button=T('Enviar'),
    )
    form.element('#tramites_procesos_documento_generado')['_class']= 'span8'
    form.element('#tramites_procesos_resumen')['_class'] = 'span8'
    form.element('#tramites_procesos_indicacion')['_style'] = 'width: 300px; height: 300px'
    form.element('#tramites_procesos_area_destino')['_style'] = 'width: 650px; height: 300px'
    form.element('#tramites_procesos_prioridad')['_class'] = 'span2'
    form.element('#tramites_procesos_referencia')['_class'] = 'span2'

    if form.process().accepted:
        documento_generado = form.vars.documento_generado
        indicacion = form.vars.indicacion
        referencia = form.vars.referencia
        prioridad = form.vars.prioridad
        resumen = form.vars.resumen
        documento = form.vars.documento
        area_destino = form.vars.area_destino
        usuario_destino = form.vars.usuario_destino
        comentarios = form.vars.comentarios
        #file_type = request.vars.documento.type
        file_name = request.vars.documento.filename
        #print documento, file_type, file_name
        data_tramite = {
            'documento_generado': documento_generado,
            'referencia': referencia,
            'prioridad': prioridad,
            'area': user_area,
            'resumen': resumen
        }
        tramite = db.tramites.insert(**data_tramite)

        # Se guardo el tramite, actualizar el documento generado como no
        # pendiente
        db(db.documento_generado.id==documento_generado).update(pendiente=False)

        data_detalle = {
            'indicacion': indicacion,
            'tramite': tramite,
            'usuario': user_id,
            'documento': documento,
            'nombre_documento': file_name,
            'usuario_destino': usuario_destino,
            'area_destino': area_destino,
            'comentarios': comentarios
        }
        tramite_detalle = db.tramites_procesos.insert(**data_detalle)

        if ALFRESCO:
            default_dir = repo.getObjectByPath('/tramite_documentario')
            default_dir = default_dir.createFolder('tramite_%s' % tramite)
            someFile = open(documento, 'r')
            someDoc = default_dir.createDocument(file_name, contentFile=someFile)

        redirect(URL('procesos','desarrollo_tramite', vars=dict(tramite=tramite)))
    return dict(form=form, form_document=form_document)

def review_desarrollo_tramite(form):
    data = form.vars
    documento_generado = db(db.documento_generado.id==form.vars.documento_generado)
    documento_generado.update(pendiente=False)
    if data['usuario_destino'] == '' or data['usuario_destino'] == '0':
        data['usuario_destino'] = None
    if data['area'] == '' or data['area'] == '0':
        data['area'] = None


@auth.requires_login()
def desarrollo_tramite():
    user_id = auth.user.id
    user_area = auth.user.area
    tramite = request.vars.tramite
    documento_numeracion_id = None

    if tramite is None:
        if len(request.args) >= 3:
            tramite = request.args[2]
        else:
            tramite = None
    db.tramites.estado.writable = False
    db.tramites_procesos.tramite.writable = False
    db.tramites_procesos.usuario.default = user_id
    db.tramites_procesos.usuario.writable = False
    create = False
    if tramite is not None:
        query = (db.tramites.id==tramite)
        create = True
    else:
        query = (db.tramites.id>0)
    conditional = {'tramites':query}

    # Formulario de generación de documentos
    form_document = SQLFORM.factory(
        Field('tipo_documento', db.tipos_tramites, label=T('Tipo Documento'),
            requires=IS_IN_DB(db, db.tipos_tramites, '%(nombre)s', zero='---Seleccionar---')),
        _class='form-inline',
        _name='form-document',
        submit_button='Generar',
        formstyle='inline',
    )
    form_document.element('#no_table_tipo_documento')['_class'] = 'span5'

    if form_document.process().accepted:
        tipo_documento = form_document.vars.tipo_documento
        area = auth.user.area
        if area:
            documento_numeracion_id = generacion_documento(tipo_documento, area)
            response.flash = 'Se generó el siguiente documento: %s' % documento_numeracion_id.numero_generado
        else:
            response.flash = 'ERROR: ¡Usuario sin área!'

    db.tramites_procesos.documento_generado.default = documento_numeracion_id
    db.tramites_procesos.documento_generado.requires = IS_EMPTY_OR(IS_IN_DB(db((db.documento_generado.pendiente==True) & (db.documento_generado.usuario==user_id)), db.documento_generado, '%(numero_generado)s'))

    form = SQLFORM.smartgrid(db.tramites,
        divider=' ⇒',
        linked_tables=['tramites_procesos'],
        fields=[
            db.tramites.id,
            db.tramites.tiempo,
            db.tramites.documento_generado,
            db.tramites.referencia,
            db.tramites.prioridad,
            db.tramites.area,
            db.tramites.resumen,
            db.tramites.estado,
            db.tramites_procesos.tiempo,
            db.tramites_procesos.indicacion,
            db.tramites_procesos.documento_generado,
            db.tramites_procesos.documento,
            db.tramites_procesos.usuario,
            db.tramites_procesos.area_destino,
            db.tramites_procesos.usuario_destino,
            db.tramites_procesos.comentarios
        ],
        editable=False,
        deletable=False,
        create=create,
        constraints=conditional,
        onvalidation=review_desarrollo_tramite,
        exportclasses=dict(
            xml=False,
            html=False,
            json=False,
            tsv=False,
            tsv_with_hidden_cols=False
        ),
        maxtextlength=100,
        _class='form form-inline',
        formstyle='bootstrap'
    )

    if form.create_form:
        form.element('#tramites_procesos_documento_generado')['_class']= 'span8'
        form.element('#tramites_procesos_indicacion')['_style'] = 'width: 300px; height: 300px'
        form.element('#tramites_procesos_area_destino')['_style'] = 'width: 650px; height: 300px'
        form.element('#tramites_procesos_estado')['_class'] = 'span2'
    elif form.view_form:
        # Customizacion de vista
        pass
    else:
        if form.element('.web2py_htmltable table'):
            form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
        if form.element('.web2py_table table tbody'):
            # Colores para las prioridades
            for tr in form.element('.web2py_table table tbody'):
                for td in tr.components:
                    if 'Urgente' in td.components:
                        td.add_class('warning warning-message')
                    elif 'Muy Urgente' in td.components:
                        td.add_class('danger danger-message')
    return dict(form=form, form_document=form_document, tramite=tramite)

@auth.requires_login()
def consulta_tramite():
    user_id = auth.user.id
    user_area = auth.user.area
    db.tramites.estado.writable = False
    db.tramites_procesos.tramite.writable = False
    db.tramites_procesos.usuario.default = user_id
    db.tramites_procesos.usuario.writable = False
    form = SQLFORM.smartgrid(db.tramites,
        divider=' ⇒',
        fields=[
            db.tramites.id,
            db.tramites.tiempo,
            db.tramites.tipo_tramite,
            db.tramites.referencia,
            db.tramites.prioridad,
            db.tramites.area,
            db.tramites.resumen,
            db.tramites.estado,
            db.tramites_procesos.id,
            db.tramites_procesos.tiempo,
            db.tramites_procesos.indicacion,
            db.tramites_procesos.usuario,
            db.tramites_procesos.area_destino,
            db.tramites_procesos.usuario_destino,
            db.tramites_procesos.comentarios
        ],
        linked_tables = ['tramites_procesos'],
        editable=False,
        deletable=False,
        create=False,
        exportclasses=dict(xml=False, html=False, json=False, tsv=False, tsv_with_hidden_cols=False),
        maxtextlength=100,
        formstyle='bootstrap'
    )
    return dict(form=form)

@auth.requires_login()
def generacion_documento(tipo_documento, area):
    documento_numeracion = db(
        (db.documento_numeracion.tipo_documento==tipo_documento) & (db.documento_numeracion.area==area)
    ).select().first()
    if not documento_numeracion:
        # Primer registro de esta area para este tipo de documento
        data = {
            'tipo_documento': tipo_documento,
            'area': area,
        }
        documento_numeracion = db.documento_numeracion.insert(**data)
    else:
        # Ya existe el registro, actualizar contador y año
        db(db.documento_numeracion.id==documento_numeracion.id).update(
            numero=documento_numeracion.numero+1,
            ano=datetime.datetime.now().year
        )
    data = {
        'documento_numeracion': documento_numeracion.id,
        'numero_generado': '%s %s-%s-IN/SALUDPOL-%s' % (
            documento_numeracion.tipo_documento.nombre,
            str(documento_numeracion.numero).zfill(4),
            documento_numeracion.ano,
            documento_numeracion.area.siglas,
        ),
        'usuario': auth.user
    }
    return db.documento_generado.insert(**data)

