# -*- coding: utf-8 -*-
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

@auth.requires_membership('root')
def tipos_tramites():
    form = SQLFORM.grid(db.tipos_tramites, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)

@auth.requires_membership('root')
def documentos():
    form = SQLFORM.grid(db.documento_numeracion, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)

@auth.requires_membership('root')
def areas():
    form = SQLFORM.grid(db.areas, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)

@auth.requires_membership('root')
def prioridades():
    form = SQLFORM.grid(db.prioridades, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)

@auth.requires_membership('root')
def indicaciones():
    form = SQLFORM.grid(db.indicaciones, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)

@auth.requires_membership('root')
def users_manage():
    form = SQLFORM.grid(db.auth_user, csv=False, deletable=False, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)

#@auth.requires(restrictions)
@auth.requires_membership('root')
def users_groups():
    form = SQLFORM.grid(db.auth_group, csv=False, deletable=False, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)

#@auth.requires(restrictions)
@auth.requires_membership('root')
def users_membership():
    form = SQLFORM.grid(db.auth_membership, csv=False, deletable=False, maxtextlength=100, formstyle='bootstrap')
    if form.element('.web2py_htmltable table'):
        form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(form=form)
