# -*- coding: utf-8 -*-
### required - do no delete
def user():
    return dict(form=auth())

def download():
    return response.download(request,db)

def call():
    return service()
### end requires

def error():
    return dict()

@auth.requires_login()
def ocultar_archivo():
    archivo_id = request.args(0)
    db(db.archivos.id==archivo_id).update(oculto=True)
    redirect(URL('archivos', 'lista_archivos'))

@auth.requires_login()
def compartir_archivos():
    default = request.args(0)
    db.archivos.archivo.writable = False
    db.archivos.archivo.readable = False
    form = SQLFORM.factory(
        Field('archivo', db.archivos, label='Archivo', requires=IS_IN_DB(db((db.archivos.usuario==auth.user) & (db.archivos.oculto==False)), db.archivos, '%(nombre_archivo)s'), default=default),
        Field('compartido_con', 'list:reference auth_user', label='Compartir con', requires=IS_IN_DB(db(db.auth_user.id!=auth.user.id), db.auth_user, '%(last_name)s, %(first_name)s', multiple=True)),
        Field('mensaje', 'text', label='Mensaje'),
        table_name='archivos_compartidos',
        formstyle='bootstrap',
        submit_button=T('Compartir'),
    )
    # form.element('#archivos_compartidos_archivo')['_style'] = 'width: 100%'
    form.element('#archivos_compartidos_compartido_con')['_style'] = 'height: 300px'

    if form.process().accepted:
        data = {
            'archivo': form.vars.archivo,
            'compartido_con': form.vars.compartido_con,
            'mensaje': form.vars.mensaje,
        }
        db.archivos_compartidos.insert(**data)
        session.flash = 'Archivo compartido'
        redirect(URL('archivos', 'lista_archivos'))
    return dict(form=form)

@auth.requires_login()
def lista_archivos():
    # Archivos subidos por el usuario
    personales = SQLFORM.grid(
        db((db.archivos.usuario==auth.user) & (db.archivos.oculto==False)).query,
        fields=[
            db.archivos.archivo,
            db.archivos.notas
        ],
        links=[
            dict(header='Acciones', body=lambda row: DIV(
                A(IMG(_src=URL('static','images/share_icon.png'), _title='Compartir'), _href=URL('archivos', 'compartir_archivos', args=row.id), _style='margin-right:20px;', _class='tooltip-title'),
                A(IMG(_src=URL('static','images/edit_icon.png'), _title='Editar'), _href=URL('archivos', 'nuevo_archivo', args=row.id), _style='margin-right:20px;', _class='tooltip-title'),
                A(IMG(_src=URL('static','images/delete_icon.png'), _title='Eliminar'), _href=URL('archivos', 'ocultar_archivo', args=row.id), _style='margin-right:20px;', _class='tooltip-title delete-user-file')
            )),
        ],
        create=False,
        editable=False,
        deletable=False,
        details=False,
        csv=False,
        maxtextlength=500,
        orderby=db.archivos.nombre_archivo
    )

    # Archivos compartidos con el usuario
    compartidos = SQLFORM.grid(
        db(db.archivos_compartidos.compartido_con.belongs([auth.user.id])).query,
        fields=[
            db.archivos_compartidos.archivo,
            db.archivos_compartidos.mensaje,
        ],
        create=False,
        editable=False,
        deletable=False,
        details=False,
        csv=False,
        maxtextlength=500,
        left=db.archivos.on(db.archivos.id==db.archivos_compartidos.archivo),
        orderby=db.archivos.nombre_archivo
    )

    for form in [personales, compartidos]:
        if form.element('.web2py_htmltable table'):
            form.element('.web2py_htmltable table')['_class'] = 'table table-bordered'
    return dict(personales=personales, compartidos=compartidos)

@auth.requires_login()
def nuevo_archivo():
    if request.args:
        title = 'Editar archivo'
        db.archivos.id.readable= False
        db.archivos.archivo.writable = False
        db.archivos.nombre_archivo.readable = False
        db.archivos.nombre_archivo.writable = False
        db.archivos.usuario.readable = False
        db.archivos.usuario.writable = False
        db.archivos.oculto.readable = False
        db.archivos.oculto.writable = False
        form = SQLFORM(db.archivos, request.args[0], formstyle='bootstrap',)
        if form.process().accepted:
            redirect(URL('archivos', 'lista_archivos'))
    else:
        title = 'Nuevo archivo'
        form = SQLFORM.factory(
            Field('archivo', 'upload', label=T('Archivo'), uploadfolder=UPLOAD_PATH, requires=IS_NOT_EMPTY(error_message='Debe adjuntar un archivo')),
            Field('notas', 'text', label=T('Notas')),
            _name='form-document',
            submit_button='Subir',
            formstyle='bootstrap',
            table_name='archivos'
        )
        if form.process().accepted:
            data = {
                'archivo': form.vars.archivo,
                'nombre_archivo': request.vars.archivo.filename,
                'notas': form.vars.notas,
                'usuario': auth.user,
            }
            db.archivos.insert(**data)
            redirect(URL('archivos', 'lista_archivos'))
    return dict(form=form, title=title)
