# -*- coding: utf-8 -*-
import datetime
import gluon.contrib.simplejson
### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires

def error():
    return dict()

def get_items():
    MINCHARS = 2 # characters required to trigger response
    MAXITEMS = 20 # number of items in response
    query = request.vars.q
    base = request.vars.base
    fields = request.vars.content
    format = request.vars.format
    aux = request.vars.aux_base
    aux_table = False
    aux_field = False
    if len(aux) > 0:
        tmp_aux = aux.split(':')
        aux_origin = tmp_aux[0]
        aux_table = tmp_aux[1]
        aux_field = tmp_aux[2]
    else:
        aux_origin = None
        aux_table = None
        aux_field = None
    id = db[base]['id']
    if len(query.strip()) > MINCHARS and fields and base:
        data_fields = [db[base][field] for field in format.split(',')]
        content = [db[base][field] for field in fields.split(',')]
        condition = [field.contains(query) for field in content]
        data = []
        for cond in condition:
            data.extend([ row for row in db(cond).select(id, *data_fields,
                limitby=(0, MAXITEMS)).as_list()])
        elements=[]
        for line in data:
            tmp=[]
            for field in format.split(','):
                content = line[field]
                if field == aux_origin:
                    content = db[aux_table](db[aux_table]['id']==content)[aux_field]
                tmp.append(content)
            #elements.append({'uuid':line['id'], 'value':"%s - %s" % tuple(tmp)})
            elements.append({'uuid':line['id'], 'value': "-".join(tmp)})
            #elements[line['id']] = "%s - %s" % tuple(tmp)
    else:
        elements = {}
    return gluon.contrib.simplejson.dumps(elements)


@auth.requires_login()
def index():
    user_id = auth.user.id
    user_area = auth.user.area
    if not user_area:
        response.flash = SPAN('No tiene un área asignada. Comuníquese con el área de Sistemas', _class='text-warning')
        return dict(area_data=[], user_data=[])
    com_data = db((db.tramites.id>0) & (db.tramites.estado==0)).select(
        db.tramites.id,
        db.tramites_procesos.id,
        db.tramites_procesos.tiempo,
        db.tramites.documento_generado,
        db.tramites.prioridad,
        db.tramites.area,
        db.tramites_procesos.indicacion,
        db.tramites.resumen,
        db.tramites_procesos.area_destino,
        db.tramites_procesos.usuario_destino,
        left=db.tramites_procesos.on(db.tramites_procesos.tramite==db.tramites.id),
        orderby=db.tramites.id|db.tramites_procesos.id
    )
    data = {}
    data_area = []
    data_usuario = []
    for row in com_data:
        documento_generado = row.tramites.documento_generado
        prioridad = row.tramites.prioridad
        area = row.tramites.area
        indicaciones = row.tramites_procesos.indicacion
        area_destino = row.tramites_procesos.area_destino
        usuario_destino = row.tramites_procesos.usuario_destino
        nombre_documento_generado = db.documento_generado(db.documento_generado.id==documento_generado)['numero_generado']
        nombre_prioridad = db.prioridades(db.prioridades.id==prioridad)['nombre']
        try:
            nombre_area = db.areas(db.areas.id==area)['nombre']
        except:
            nombre_area = ''
        try:
            nombres = []
            for indicacion in indicaciones:
                nombres.append(LI(indicacion.nombre))
            nombre_indicacion = UL(*nombres)
        except:
            nombre_indicacion = ''
        content = {
            'proceso': row.tramites_procesos.id,
            'tiempo': row.tramites_procesos.tiempo.strftime('%d/%m/%Y %H:%M:%S'),
            'fecha': row.tramites_procesos.tiempo,
            'documento_generado': nombre_documento_generado,
            'prioridad': nombre_prioridad,
            'resumen': row.tramites.resumen,
            'area': nombre_area,
            'indicacion': nombre_indicacion,
            'area_destino': area_destino,
            'usuario_destino': usuario_destino
        }
        data[row.tramites.id] = content
    for line in data:
        dias_demora = (datetime.datetime.now() - data[line]['fecha']).days
        if dias_demora >= 15:
            _class = 'delay-red'
        elif dias_demora > 7 and dias_demora < 15:
            _class = 'delay-orange'
        else:
            _class = 'delay-green'
        demora = DIV(B('%s días' % dias_demora), _class=_class)
        tmp = [
            str(line).zfill(4),
            data[line]['tiempo'],
            data[line]['area'],
            data[line]['documento_generado'],
            data[line]['prioridad'],
            data[line]['resumen'],
            data[line]['indicacion'],
            demora
        ]
        if int(user_area) in data[line]['area_destino']:
            data_area.append(tmp)
        if data[line]['usuario_destino'] == user_id:
            data_usuario.append(tmp)
    return dict(area_data=data_area, user_data=data_usuario)
